#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object

const int fwd_speed = 200;       // Default forward speed
const int turn_speed_slow = 130; // Speed of slower motor during a turn
const int turn_speed_fast = 230;  // Speed of faster motor

// Declare pins for sensors and claw servos
const int right_sensor_pin = 2; 
const int left_sensor_pin = 4;
const int sensors_power = 7;

const int start_button = 6;

bool left_sensor; // Left line follower sensor
bool old_left_sensor;
bool right_sensor; // Right line follower sensor
bool old_right_sensor;

int counter = 0;

void setup()
{
  AFMS.begin(); // Connecting to the controller

  pinMode(start_button, INPUT);


  Serial.begin(9600); // For debugging

  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);
  
  pinMode(sensors_power, OUTPUT);

  // Speed ranges from 0(stopped) to 255(full speed)
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);

  old_left_sensor = true;
  old_right_sensor = true;
}

// Read sensor values for the line follower sensors
void read_line_sensors()
{
  digitalWrite(sensors_power, HIGH);
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
  Serial.print(left_sensor);
  Serial.println(right_sensor);
}

// Turn the robot to the right while still moving
void rightTurn()
{
  Serial.println("Turning Right");
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
}
// Turn the robot to the left while still moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
}
// Make the robot move in the direction its pointing
void goStraight()
{
  Serial.println("Going forward");
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);
}

void choose_movement(int left, int right)
{
  if (left == false && right == false)
  {
    goStraight();
  }
  else if (left == true && right == false)
  {
    leftTurn();
  }
  else if (left == false && right == true)
  {
    rightTurn();
  }
  else if (left == true && right == true)
  {
    goStraight();
  }
  old_left_sensor = left;
  old_right_sensor = right;
}


void stop_robot() {
  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);
}

void loop(){
  
  //Fix this in void loop()

  // get sensor data
  digitalWrite(sensors_power, HIGH);
  read_line_sensors();
  
  
  // if sensors changed, change speed of motors
  if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor))
  {
    choose_movement(left_sensor, right_sensor);
  }
}
