// COLOUR SENSOR AND LEDs
int colour_sensor_pin = A0; // Declaring colour sensor pin
const int redLED = 5;  // Declaring red LED pin
const int blueLED = 8; // Declaring blue LED pin
int colour_sensor_reference=0; // Reference obtained by calibrating the colour sensor 
bool isBlockRed; // Boolean set to true if the block is red, otherwise false

void setup() {
  Serial.begin(9600); // For debugging

  pinMode(colour_sensor_pin, INPUT);
  pinMode(redLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
}

float calibrate_colour_sensor(){
  float total_value = 0;

  for (int t=0; t<100; t++){
    if(analogRead(A0)<1000){
      total_value = abs(analogRead(colour_sensor_pin)) + total_value;
    }
    else{
      t--;
    } 
  }
  Serial.println(float(total_value/100));
  return (total_value/100);
}

void blockColour(){
  int v = analogRead(colour_sensor_pin);

  if (v < colour_sensor_reference - 100){
    digitalWrite(redLED, HIGH);
    digitalWrite(blueLED, LOW);
    isBlockRed = true;
  }
  else {
    digitalWrite(blueLED, HIGH);
    digitalWrite(redLED, LOW);
    isBlockRed = false;
  }
}

void loop() {
  if (colour_sensor_reference == 0){
    colour_sensor_reference = calibrate_colour_sensor();
  }

  blockColour();

}
