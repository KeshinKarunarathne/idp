#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

// ADAFRUIT MOTORSHIELD
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object
const int fwd_speed = 240;                          // Default forward speed
const int turn_speed_slow = 120;                    // Speed of slower motor during a turn
const int turn_speed_fast = 240;                    // Speed of faster motor during a turn
const int time_interval = 50;
int right_sensor;
int left_sensor;
int old_left_sensor;
int old_right_sensor;
int sensor_change_count = 0;


void setup {
    AFMS.begin(); // Connecting to the controller

    Serial.begin(9600); // For debugging

    pinMode(right_sensor_pin, INPUT);
    pinMode(left_sensor_pin, INPUT);

    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
    leftMotor->setSpeed(turn_speed_low);
    rightMotor->setSpeed(turn_speed_low);
    old_left_sensor = LOW;
    old_right_sensor = LOW;
}

void read_line_sensors()
{
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
}
void loop {
    read_line_sensors();
    if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)) {
        sensor_change_count++;
        if (sensor_change_count > 3) {
            leftMotor->setSpeed(0);
            rightMotor->setSpeed(0);
        }
    }

}
