#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

// Initially we had planned on a sweep. Most of the code for this is written but we simply did not have the time to
// test and implement it as we kept having small issues to fix with the robot
// All the sweep code is not in this file, it is in a different file that will be provided (however it won't be annotated)

// ADAFRUIT MOTORSHIELD
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object
const int fwd_speed = 250;                          // Default forward speed
const int turn_speed_slow = 120;                    // Speed of slower motor during a turn
const int turn_speed_fast = 250;                    // Speed of faster motor during a turn
const int pivot_speed = 200;                        // Speed of both motors while pivoting
const int time_interval = 50;                       // Time inverval for loop is 50 ms

// SERVO
Servo servo_claw;                // Declaring first servo object
Servo servo_lift;                // Declaring second servo object
const int servo_claw_pin = 9;    // Servo controlling claw
const int servo_lift_pin = 10;   // Servo controlling pitch of claw
int servo_claw_pos_open = 100;   // Position of the servo powering the claw
int servo_claw_pos_closed = 145; // Took some trial and error to find these values
int servo_lift_pos_up = 60;      // Position of the claw pitch servo
int servo_lift_pos_down = 160;

// COLOUR SENSOR AND LEDs
int colour_sensor_pin = A0;      // Declaring colour sensor pin
const int redLED = 7;            // Declaring red LED pin
const int greenLED = 6;          // Declaring green LED pin
int colour_sensor_reference = 0; // Reference obtained by calibrating the colour sensor
bool isBlockRed = true;          // Boolean set to true if the block is red, otherwise false. True by default, mostly for debugging

// LINE FOLLOWERS
const int right_sensor_pin = 4;  // Right line sensor
const int left_sensor_pin = 5;   // Left line sensor
bool left_sensor;                // Left line follower sensor state
bool old_left_sensor;            // Keeps track of old value for comparison
bool right_sensor;               // Right line follower sensor state
bool old_right_sensor;           // Keeps track of old value for comparison
int lines_passed = 0;            // Tracks number of horizontal stripes passed
bool on_horizontal_line = false; // State that determines if it is currently above a stripe
bool sensors_high = false;       // Will be true if both line sensors are on the line (used to detect junctions)
int points_crossed = 0;          // Number of times we crossed a junction

// ULTRASOUND
int echo_pin = 12; // Define pins for trig and echo for ultrasonic sensor
int trig_pin = 11;
long ultrasound_duration; // These variables will be used later to calculate distance
int ultrasound_distance;

// "MODE" BOOLEANS
// All of these booleans are used to decide the "mode" the robot is in during every iteration of the main loop
// Most of them are relatively self-explanatory, the rest are annotated
bool line_follower_running = true; // We are running the line follower (this will still run alongside other modes)
bool calibrate = false;
bool final_approach = false; // We are getting close to the block, using ultrasound
bool colour_identification = false;
bool block_snatch = false;                 // Grab block, move back slightly and turn around
bool line_follower_running_return = false; // We are running the line follower on our way back to drop-off
bool return_encoder = false;               // We are measuring distance until the 90 degree turn using the encoders, and turning around
bool approach_block = false;               // Move until the dropoff junction is found
bool go_back_dropoff = false;              // Move back using encoders to a calibrated distance so that block is dropped off
bool return_to_line = false;               // Move back and turn around to find line, go back to the start

bool first_run = true; // Used to mark if its the first block or not

bool go_slow = false; // Used to mark if the robot is moving at a slower speed
// This was only really necessary because we never planned to use the line follower to move slowly, so we had to add a quick fix after for when it is

bool enough_points_crossed = false; // Used to change from line follower to the appropiate mode once enough junctions are passed

int button_pin = 8;
bool start_code = false; // Used for the button

// ENCODERS
const int left_encoder_pin = 3; // Encoder pin (has to be pin 2 or 3 for Arduino Uno)
const int right_encoder_pin = 2;
int left_encoder_count = 0;  // Number of times the left photointerrupter changes state
int right_encoder_count = 0; // Number of times the right photointerrupter changes state
float average_encoder_count = 0.5 * (left_encoder_count + right_encoder_count);

const float encoder_wheel_radius = 4.0;                        // Radius of encoder wheel
const float robot_wheel_radius = 5.25;                         // Radius of robot wheels
const float distance_between_wheels = 25.7;                    // Distance between robot wheels
const float pivot_circle_radius = distance_between_wheels / 2; // Radius of pivot circle
float pivot_arc;                                               // The arc through which each wheel should turn in pivotAngle()
int target_encoder_count = 0;                                  // Encoder count required to turn a certain pivot angle
const int encoder_holes = 36;                                  // Number of holes in the encoder wheel
// Various distances used as a reference for encoder calculations
float distance_travelled;
float distance_from_line_sensor_to_wheels = 15.5;
float distance_from_marker_to_drop_off_line = 18.0;
float distance_to_90_deg_turn = distance_from_line_sensor_to_wheels + distance_from_marker_to_drop_off_line;
const float distance_back_dropoff = 9.0;
const float distance_to_line = 27.5;

void setup()
{
    AFMS.begin(); // Connecting to the controller

    Serial.begin(9600); // For debugging
    pinMode(button_pin, INPUT);

    pinMode(right_sensor_pin, INPUT);
    pinMode(left_sensor_pin, INPUT);

    pinMode(trig_pin, OUTPUT);
    pinMode(echo_pin, INPUT);

    // Attach the servos
    servo_claw.attach(servo_claw_pin);
    servo_claw.write(servo_claw_pos_open);
    servo_lift.attach(servo_lift_pin);
    servo_lift.write(servo_lift_pos_up);

    pinMode(colour_sensor_pin, INPUT);
    pinMode(redLED, OUTPUT);
    pinMode(greenLED, OUTPUT);

    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);

    old_left_sensor = true;
    old_right_sensor = true;

    // Use interrupts for encoders
    attachInterrupt(digitalPinToInterrupt(left_encoder_pin), left_encoder_counter, CHANGE);
    attachInterrupt(digitalPinToInterrupt(right_encoder_pin), right_encoder_counter, CHANGE);

    left_encoder_count = 0;
    right_encoder_count = 0;

    digitalWrite(redLED, LOW);
    digitalWrite(greenLED, LOW);
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);
    // Code will stay in setup until the start button is pressed
    while (!start_code)
    {
        if (digitalRead(button_pin) == LOW)
        {
            start_code = true;
        }

        delay(50);
    }
    leftMotor->setSpeed(fwd_speed);
    rightMotor->setSpeed(fwd_speed);
}

// Read the line sensor, HIGH for on the line and LOW for no line
void read_line_sensors()
{
    left_sensor = digitalRead(left_sensor_pin);
    right_sensor = digitalRead(right_sensor_pin);
}

// Turn the robot to the right while still moving
void rightTurn()
{
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_fast);
}
// Turn the robot to the left while still moving
void leftTurn()
{
    rightMotor->setSpeed(turn_speed_fast);
    leftMotor->setSpeed(turn_speed_slow);
}
// Make the robot move in the direction its pointing
void goStraight()
{
    rightMotor->setSpeed(fwd_speed);
    leftMotor->setSpeed(fwd_speed);
    // if we are no longer on the line, increase the line counter
    if (on_horizontal_line == true)
    {
        lines_passed++;
        on_horizontal_line = false;
    }
    // If we are meant to be going slow, use the slow speed
    if (go_slow)
    {
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
    }
}
// Used to determine the movement of the robot
void choose_movement(int left, int right)
{
    if (left == false && right == false)
    {
        goStraight();
        // If we were previously on a junction/line
        if (sensors_high == true)
        {
            sensors_high = false;
            points_crossed++;
            Serial.print("Crossed: ");
            Serial.println(points_crossed);
            Serial.println(distanceTravelled());
            // If we are returning, only one junction has to be crossed. Distance travelled is there to avoid misfires (more detail in the documentation/report)
            if (line_follower_running_return && points_crossed >= 1 && distanceTravelled() > 50.0)
            {
                Serial.println("RETURN");
                return_encoder = true;
            }
            // If not calibrating or approaching the block
            if ((!calibrate && !final_approach))
            {
                // on the first block, there will be 3 junctions, but on the next few only 1 (we did not have time to implement the sweep and subsequent blocks)
                if (!first_run && points_crossed >= 1 || (points_crossed >= 3 && distanceTravelled() > 170.0))
                {
                    enough_points_crossed = true;
                }
            }
        }
    }
    else if (left == true && right == false)
    {
        leftTurn();
    }
    else if (left == false && right == true)
    {
        rightTurn();
    }
    else if (left == true && right == true)
    {
        goStraight();
        sensors_high = true;
    }
    // we keep track of the previous sensors, and only call this function if the sensors have changed (to avoid unnecesarily calling it 20 times per second)
    old_left_sensor = left;
    old_right_sensor = right;
}

// Encoder functions for interrupts
void left_encoder_counter()
{
    left_encoder_count++;
}

void right_encoder_counter()
{
    right_encoder_count++;
}

// Uses encoder count and data collected to measure distance
float distanceTravelled()
{
    // float average_encoder_count = 0.5*(left_encoder_count+right_encoder_count);
    distance_travelled = 4 * PI * robot_wheel_radius * (0.5 * (left_encoder_count + right_encoder_count) / (encoder_holes * 2));
    return distance_travelled;
}

// Used to turn the robot. The parameter number is the number of line crossings that will be encountered (we use the line sensors to turn)
void turn_around(int number, bool reverse)
{

    if (!reverse)
    {
        leftMotor->run(BACKWARD);
        rightMotor->run(FORWARD);
    }
    else
    {
        leftMotor->run(FORWARD);
        rightMotor->run(BACKWARD);
    }
    leftMotor->setSpeed(turn_speed_slow + 50);
    rightMotor->setSpeed(turn_speed_slow + 50);

    int turn_line_crossings = 0;
    while (turn_line_crossings < number)
    {
        read_line_sensors();
        // The main purpose is that if any of the line sensors have changed, then we will mark that as a line crossing
        // This code is a bit messy. The purpose of using encoders in some specific cases is because when turning we would get misfires on the line sensors
        // which resulted in the turn ending early, so we used a minimum distance travelled to ensure it didn't happen
        // not the cleanest solution but at the time of implementation we did not have much time to rewrite a lot of our code, and it worked great
        if ((!(left_sensor == old_left_sensor && right_sensor == old_right_sensor) && !block_snatch && !return_to_line) || (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor) && block_snatch && distanceTravelled() > 30.0) || (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor) && return_to_line && distanceTravelled() > 15.0))
        {
            turn_line_crossings++;
            Serial.print(distanceTravelled());
            Serial.print("        ");
            Serial.println(turn_line_crossings);
        }
        old_left_sensor = left_sensor;
        old_right_sensor = right_sensor;
        delay(time_interval);
    }
    Serial.println("Turn over");
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
}

// Used to move after the block has been picked up, robot moves back and turns around
void move_forwards_pickup()
{
    servo_claw.write(servo_claw_pos_closed);
    delay(300);
    servo_lift.write(servo_lift_pos_up);

    rightMotor->run(BACKWARD);
    leftMotor->run(BACKWARD);
    rightMotor->setSpeed(fwd_speed);
    leftMotor->setSpeed(fwd_speed);

    delay(3000);

    left_encoder_count = 0;
    right_encoder_count = 0;

    turn_around(2, false);
    read_line_sensors();
    choose_movement(left_sensor, right_sensor);
}

// Very standard (might be the example even) code to read ultrasound distance
int get_ultrasound_reading()
{
    digitalWrite(trig_pin, LOW);
    delayMicroseconds(2);
    digitalWrite(trig_pin, HIGH);
    delayMicroseconds(10);

    digitalWrite(trig_pin, LOW);
    ultrasound_duration = pulseIn(echo_pin, HIGH);

    ultrasound_distance = (ultrasound_duration * 0.034 / 2) + 1;
    Serial.println(ultrasound_distance);
}

// Used to take a reference for the colour sensor. This is done while the claw is pointing down, and considerably before the block is encountered
float calibrate_colour_sensor()
{
    float total_value = 0;

    for (int t = 0; t < 100; t++)
    {
        if (analogRead(A0) < 1000)
        {
            total_value = abs(analogRead(colour_sensor_pin)) + total_value;
        }
        else
        {
            t--;
        }
    }
    return (total_value / 100);
}

// Used to determine the colour of the block
void blockColour()
{
    int v = analogRead(colour_sensor_pin);

    if (v < colour_sensor_reference - 100)
    {
        digitalWrite(redLED, HIGH);
        digitalWrite(greenLED, LOW);
        isBlockRed = true;
        Serial.println("RED");
    }
    else
    {
        digitalWrite(greenLED, HIGH);
        digitalWrite(redLED, LOW);
        isBlockRed = false;
        Serial.println("BLUE");
    }
}

// Main loop
void loop()
{
    read_line_sensors();
    // Only choose movement if the sensors have changed
    if ((line_follower_running) && (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)))
    {
        choose_movement(left_sensor, right_sensor);
        // If enough points were crossed, start calibration
        if (enough_points_crossed)
        {
            servo_lift.write(servo_lift_pos_down);
            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);

            points_crossed = 0;
            // At the end of these we usually make the next "mode" boolean true and the current "mode" boolean false
            calibrate = true;
            enough_points_crossed = false;
        }
    }
    else if (calibrate)
    {
        // If there is no reference, calibrate while the claw is down
        if (colour_sensor_reference == 0)
        {
            rightMotor->setSpeed(0);
            leftMotor->setSpeed(0);
            servo_claw.write(servo_claw_pos_closed);
            servo_lift.write(servo_lift_pos_down);
            delay(500);
            colour_sensor_reference = calibrate_colour_sensor();
            delay(500);
            servo_claw.write(servo_claw_pos_open);
            servo_lift.write(servo_lift_pos_up);
            delay(500);
            go_slow = true;
            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);
        }
        // Engage final approach
        final_approach = true;
        calibrate = false;
    }
    // Uses ultrasound until it is 14cm away
    else if (final_approach)
    {
        line_follower_running = true;
        get_ultrasound_reading();
        if (ultrasound_distance < 14)
        {
            Serial.println("Distance is less than 12");
            rightMotor->setSpeed(0);
            leftMotor->setSpeed(0);
            colour_identification = true;
            final_approach = false;
        }
        // This ensures the 50ms loop still happens. This was unimportant at the end, as we used a timing circuit for the amber LED
        delay(time_interval - 12);
        return;
    }
    // Identifies the colour of the block
    else if (colour_identification)
    {
        servo_claw.write(servo_claw_pos_closed);
        servo_lift.write(servo_lift_pos_down);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        delay(500);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        blockColour();
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        delay(5000);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        block_snatch = true;
        colour_identification = false;
    }
    // Grabs the block
    else if (block_snatch)
    {
        go_slow = false;
        servo_lift.write(servo_lift_pos_down);
        delay(300);
        servo_claw.write(servo_claw_pos_open);
        delay(300);
        leftMotor->setSpeed(fwd_speed);
        rightMotor->setSpeed(fwd_speed);
        delay(1500);
        rightMotor->setSpeed(fwd_speed);
        leftMotor->setSpeed(fwd_speed);
        move_forwards_pickup();
        line_follower_running_return = true;
        line_follower_running = true;
        left_encoder_count = 0;
        right_encoder_count = 0;
        block_snatch = false;
    }
    // Uses line follower on the way back
    else if (line_follower_running_return && (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)))
    {

        choose_movement(left_sensor, right_sensor);
    }
    // Moves forward a set distance before stopping and turning around in the correct direction based on block colour
    else if (return_encoder)
    {
        line_follower_running_return = false;
        line_follower_running = true;
        left_encoder_count = 0;
        right_encoder_count = 0;
        Serial.println("Approaching Dropoff");
        //  We take away 2 for calibration purposes
        while (distanceTravelled() <= (distance_to_90_deg_turn - 4.0))
        {
            Serial.println(distanceTravelled());
            read_line_sensors();
            choose_movement(left_sensor, right_sensor);
            delay(time_interval);
        }
        leftMotor->setSpeed(0);
        rightMotor->setSpeed(0);
        delay(2000);
        Serial.println("In position for turn");
        if (isBlockRed)
        {
            // The reason while these turns have different values is because on the red table we kept getting misfires because of a small scuff on the table
            turn_around(6, isBlockRed); // !isBlockRed turns it in the right direction depending on block colour
        }
        else
        {
            turn_around(4, isBlockRed);
        }
        line_follower_running = false;
        delay(1000);

        approach_block = true;
        line_follower_running = false;
        return_encoder = false;
    }
    // Moves using line follower until it find the dropoff spot
    else if (approach_block)
    {
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        read_line_sensors();
        choose_movement(left_sensor, right_sensor);
        if (sensors_high)
        {
            Serial.println("Dropoff line found");
            sensors_high = false;
            rightMotor->setSpeed(0);
            leftMotor->setSpeed(0);
            delay(2000);
            leftMotor->run(BACKWARD);
            rightMotor->run(BACKWARD);

            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);
            left_encoder_count = 0;
            right_encoder_count = 0;
            go_back_dropoff = true;
            approach_block = false;
        }
    }
    // Slowly moves back a set encoder distance, and turns around a small angle to try and get the block position perfect
    else if (go_back_dropoff)
    {
        Serial.println("Going back");
        line_follower_running = false;
        if (!isBlockRed)
        {
            while (distanceTravelled() <= distance_back_dropoff - 3.5)
            {
                delay(time_interval);
            }
        }
        else
        {
            while (distanceTravelled() <= distance_back_dropoff - 2.8)
            {
                delay(time_interval);
            }
        }

        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        if (!isBlockRed)
        {
            leftMotor->run(FORWARD);
            rightMotor->run(BACKWARD);
        }
        else
        {
            rightMotor->run(BACKWARD);
            leftMotor->run(FORWARD);
        }
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        // Diferent turning angles required for different colours, likely because our motors were slightly uneven so the centre of the robot during turns would move
        if (isBlockRed)
        {
            delay(350);
        }
        else
        {
            delay(650);
        }
        // Drops off the block
        leftMotor->run(BACKWARD);
        rightMotor->run(BACKWARD);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        Serial.println("Dropping off");
        delay(2000);
        servo_lift.write(servo_lift_pos_down);
        delay(500);
        servo_claw.write(servo_claw_pos_open);
        delay(500);
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        return_to_line = true;
        go_back_dropoff = false;
    }
    // Moves back a set encoder distance to the line and turns around to return to the start. Once it finds the start line, moves forwards a small amount and stops.
    else if (return_to_line)
    {
        Serial.println("Back to line");
        while (distanceTravelled() <= distance_to_line)
        {
            Serial.println(distanceTravelled());
            delay(time_interval);
        }
        servo_claw.write(servo_claw_pos_closed);
        delay(500);
        servo_lift.write(servo_lift_pos_up);
        delay(1000);
        Serial.println("Turning");
        // The parameter 6 below works for the red table
        left_encoder_count = 0;
        right_encoder_count = 0;
        turn_around(2, !isBlockRed);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);

        Serial.println("Done");
        while (!sensors_high)
        {
            read_line_sensors();
            choose_movement(left_sensor, right_sensor);
            delay(50);
        }

        left_encoder_count = 0;
        right_encoder_count = 0;
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        rightMotor->run(FORWARD);
        leftMotor->run(FORWARD);
        while (distanceTravelled() <= 21.5)
        {
            Serial.println(distanceTravelled());
            delay(time_interval);
        }
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        first_run = false;
        return_to_line = false;
        start_code = false;
    }
    // Once the robot stops it will stay idle
    while (!start_code)
    {
        Serial.println("Idle");
        delay(100);
    }
    // Main loop delay
    delay(time_interval);
}