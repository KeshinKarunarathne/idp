#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

// ADAFRUIT MOTORSHIELD
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object
const int fwd_speed = 240;                          // Default forward speed
const int turn_speed_slow = 120;                    // Speed of slower motor during a turn
const int turn_speed_fast = 240;                    // Speed of faster motor during a turn
const int pivot_speed = 200;                        // Speed of both motors while pivoting
const int time_interval = 50;

// SERVO
Servo servo_claw;              // Declaring first servo object
Servo servo_lift;              // Declaring second servo object
const int servo_claw_pin = 9;  // Servo controlling claw
const int servo_lift_pin = 10; // Servo controlling pitch of claw
int servo_claw_pos_open = 100; // Position of the servo powering the claw
int servo_claw_pos_closed = 145;
int servo_lift_pos_up = 60; // Position of the claw pitch servo
int servo_lift_pos_down = 160;
int approach_count = 0;

// COLOUR SENSOR AND LEDs
int colour_sensor_pin = A0;      // Declaring colour sensor pin
const int redLED = 7;            // Declaring red LED pin
const int blueLED = 6;           // Declaring blue LED pin
int colour_sensor_reference = 0; // Reference obtained by calibrating the colour sensor
bool isBlockRed = true;          // Boolean set to true if the block is red, otherwise false

// LINE FOLLOWERS
const int right_sensor_pin = 4;  // Right line sensor
const int left_sensor_pin = 5;   // Left line sensor
bool left_sensor;                // Left line follower sensor state
bool old_left_sensor;            // Keeps track of old value for comparison
bool right_sensor;               // Right line follower sensor state
bool old_right_sensor;           // Keeps track of old value for comparison
int lines_passed = 0;            // Tracks number of horizontal stripes passed
bool on_horizontal_line = false; // State that determines if it is currently above a stripe

// ULTRASOUND
int echo_pin = 12;
int trig_pin = 11;
long ultrasound_duration;
int ultrasound_distance;

bool sensors_high = false;
int points_crossed = 0;
int right_turn_detection_counter = 0;

const long half_turn_duration = 4500; // Duration of 180-degree turn in ms

bool line_follower_running = true;
bool line_follower_running_return = false;
bool calibrate = false;
bool block_pickup_running = false;
bool half_turn_running = false;
bool final_approach = false;
bool colour_identification = false;
bool block_snatch = false;
bool return_encoder = false;
bool is_pivot = false;         // This is used to run pivot only once
bool is_moving_forward = true; // This is required to use the encoders for measuring distance
bool is_approaching_dropoff = false;
bool half_turn_activated = false;
bool approach_block = false;
bool go_back_dropoff = false;
bool return_to_line = false;
bool approach_sweep = false;
bool sweep_for_block = false;
bool block_found = false;

bool first_run = false;

bool go_slow = false;

bool enough_points_crossed = false;

int button_pin = 8;
bool start_code = true;

// ENCODERS
const int left_encoder_pin = 3; // Encoder pin (has to be pin 2 or 3 for Arduino Uno)
const int right_encoder_pin = 2;
int left_encoder_count = 0;  // Number of times the left photointerrupter changes state
int right_encoder_count = 0; // Number of times the right photointerrupter changes state
float average_encoder_count = 0.5 * (left_encoder_count + right_encoder_count);

const float encoder_wheel_radius = 4.0;                        // Radius of encoder wheel
const float robot_wheel_radius = 5.25;                         // Radius of robot wheels
const float distance_between_wheels = 25.7;                    // Distance between robot wheels
const float pivot_circle_radius = distance_between_wheels / 2; // Radius of pivot circle
float pivot_arc;                                               // The arc through which each wheel should turn in pivotAngle()
int target_encoder_count = 0;                                  // Encoder count required to turn a certain pivot angle
const int encoder_holes = 36;                                  // Number of holes in the encoder wheel
float distance_travelled;
float distance_from_line_sensor_to_wheels = 15.5;
float distance_from_marker_to_drop_off_line = 18.0;
float distance_to_90_deg_turn = distance_from_line_sensor_to_wheels + distance_from_marker_to_drop_off_line;
const float distance_back_dropoff = 9.0;
const float distance_to_line = 27.5;

const float min_distance_to_block = 5.0;
const float max_distance_to_block = 20.0;

void setup()
{
    AFMS.begin(); // Connecting to the controller

    Serial.begin(9600); // For debugging
                        /*
                          pinMode(button_pin, INPUT);
                    
                          while (!start) {
                            if (digitalRead(button_pin) == HIGH) {
                              start = true;
                            }
                            delay(50);
                          }
                        */
    pinMode(right_sensor_pin, INPUT);
    pinMode(left_sensor_pin, INPUT);

    pinMode(trig_pin, OUTPUT);
    pinMode(echo_pin, INPUT);

    servo_claw.attach(servo_claw_pin);
    servo_claw.write(servo_claw_pos_open);
    servo_lift.attach(servo_lift_pin);
    servo_lift.write(servo_lift_pos_up);

    pinMode(colour_sensor_pin, INPUT);
    pinMode(redLED, OUTPUT);
    pinMode(blueLED, OUTPUT);

    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
    leftMotor->setSpeed(fwd_speed);
    rightMotor->setSpeed(fwd_speed);

    old_left_sensor = true;
    old_right_sensor = true;

    attachInterrupt(digitalPinToInterrupt(left_encoder_pin), left_encoder_counter, CHANGE);
    attachInterrupt(digitalPinToInterrupt(right_encoder_pin), right_encoder_counter, CHANGE);

    left_encoder_count = 0;
    right_encoder_count = 0;

    digitalWrite(redLED, LOW);
    digitalWrite(blueLED, LOW);
}

void read_line_sensors()
{
    left_sensor = digitalRead(left_sensor_pin);
    right_sensor = digitalRead(right_sensor_pin);
    right_turn_detection_counter = 0;
}

// Turn the robot to the right while still moving
void rightTurn()
{
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_fast);
    right_turn_detection_counter++;
}
// Turn the robot to the left while still moving
void leftTurn()
{
    rightMotor->setSpeed(turn_speed_fast);
    leftMotor->setSpeed(turn_speed_slow);
    right_turn_detection_counter++;
}
// Make the robot move in the direction its pointing
void goStraight()
{
    rightMotor->setSpeed(fwd_speed);
    leftMotor->setSpeed(fwd_speed);
    right_turn_detection_counter = 0;
    // if we are no longer on the line, increase the line counter
    if (on_horizontal_line == true)
    {
        lines_passed++;
        on_horizontal_line = false;
    }
    if (go_slow)
    {
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
    }
}

void choose_movement(int left, int right)
{
    if (left == false && right == false)
    {
        goStraight();
        if (sensors_high == true)
        {
            sensors_high = false;
            points_crossed++;
            Serial.print("Crossed: ");
            Serial.println(points_crossed);
            Serial.println(distanceTravelled());
            if (line_follower_running_return && points_crossed >= 1 && distanceTravelled() > 75.0)
            {
                Serial.print("RETURN");
                Serial.println(distanceTravelled());
                return_encoder = true;
            }
            if ((!calibrate && !final_approach))
            {
                if (!first_run && points_crossed >= 1 || (points_crossed >= 3 && distanceTravelled() > 150.0))
                {
                    enough_points_crossed = true;
                }
            }
        }
    }
    else if (left == true && right == false)
    {
        leftTurn();
    }
    else if (left == false && right == true)
    {
        rightTurn();
    }
    else if (left == true && right == true)
    {
        goStraight();
        sensors_high = true;
    }
    old_left_sensor = left;
    old_right_sensor = right;
}

void left_encoder_counter()
{
    //  Serial.println(left_encoder_count);
    //  if (left_encoder_count < target_encoder_count) {
    //     left_encoder_count++;
    //  }
    left_encoder_count++;
}

void right_encoder_counter()
{
    // Serial.println(right_encoder_count);
    //  if (right_encoder_count < target_encoder_count) {
    //     right_encoder_count++;
    right_encoder_count++;
}

float distanceTravelled()
{
    // float average_encoder_count = 0.5*(left_encoder_count+right_encoder_count);
    distance_travelled = 4 * PI * robot_wheel_radius * (0.5 * (left_encoder_count + right_encoder_count) / (encoder_holes * 2));
    return distance_travelled;
}

void pivot(float pivot_angle, bool isClockwise)
{
    pivot_arc = (pivot_angle / 360.0) * 2 * PI * pivot_circle_radius;
    target_encoder_count = int(encoder_holes * 2 * (pivot_arc / (2 * PI * robot_wheel_radius)));
    Serial.println(target_encoder_count);

    if (isClockwise)
    {
        leftMotor->run(FORWARD);
        rightMotor->run(BACKWARD);
    }
    else
    {
        leftMotor->run(BACKWARD);
        rightMotor->run(FORWARD);
    }

    left_encoder_count = 0;
    right_encoder_count = 0;

    leftMotor->setSpeed(pivot_speed);
    rightMotor->setSpeed(pivot_speed);

    // Following loop had a slight systematic offset
    //  while(left_encoder_count < target_encoder_count || right_encoder_count < target_encoder_count){
    //    Serial.println(left_encoder_count);
    // }

    // Added 1.5 below just for calibration
    while (int(0.5 * (left_encoder_count + right_encoder_count + 1)) < target_encoder_count)
    {
        Serial.println(left_encoder_count);
    }

    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);

    is_pivot = false;
    Serial.println("End of Pivot");
}

void halfTurn(int number, bool reverse)
{
    // This method turns the robot by 180 degrees after grabbing a block
    if (!reverse)
    {
        leftMotor->run(BACKWARD);
        rightMotor->run(FORWARD);
    }
    else
    {
        leftMotor->run(FORWARD);
        rightMotor->run(BACKWARD);
    }
    leftMotor->setSpeed(turn_speed_slow + 50);
    rightMotor->setSpeed(turn_speed_slow + 50);

    int turn_line_crossings = 0;
    while (turn_line_crossings < number)
    {
        read_line_sensors();
        if ((!(left_sensor == old_left_sensor && right_sensor == old_right_sensor) && !block_snatch) || (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor) && block_snatch && distanceTravelled() > 30.0))
        {
            turn_line_crossings++;
            Serial.println(turn_line_crossings);
        }
        old_left_sensor = left_sensor;
        old_right_sensor = right_sensor;
        delay(time_interval);
    }
    Serial.println("Turn over");
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
}

void move_forwards_pickup()
{
    servo_claw.write(servo_claw_pos_closed);
    delay(300);
    servo_lift.write(servo_lift_pos_up);
    delay(300);

    if (first_run)
    {
        rightMotor->run(BACKWARD);
        leftMotor->run(BACKWARD);
        rightMotor->setSpeed(fwd_speed);
        leftMotor->setSpeed(fwd_speed);

        delay(3000);
        left_encoder_count = 0;
        right_encoder_count = 0;

        halfTurn(4, false);
    }
    else
    {
        rightMotor->run(BACKWARD);
        leftMotor->run(BACKWARD);
        rightMotor->setSpeed(fwd_speed);
        leftMotor->setSpeed(fwd_speed);
        delay(3000);
        rightMotor->run(FORWARD);
        leftMotor->run(FORWARD);
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        int count = 0;
        while (count < 20)
        {
            read_line_sensors();
            choose_movement(left_sensor, right_sensor);
            delay(10);
            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);
            delay(40);
            count++;
        }
        rightMotor->run(BACKWARD);
        leftMotor->run(BACKWARD);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        delay(3000);

        left_encoder_count = 0;
        right_encoder_count = 0;
        rightMotor->run(BACKWARD);
        leftMotor->run(FORWARD);
        rightMotor->setSpeed(fwd_speed);
        leftMotor->setSpeed(fwd_speed);
        while (distanceTravelled() < 55.0)
        {
            delay(time_interval);
        }
        int turn_line_crossings = 0;
        while (turn_line_crossings < 2)
        {
            read_line_sensors();
            if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor))
            {
                turn_line_crossings++;
                Serial.println(turn_line_crossings);
            }
        }
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        rightMotor->run(FORWARD);
        leftMotor->run(FORWARD);
        left_encoder_count = 0;
        right_encoder_count = 0;
    }
    read_line_sensors();
    choose_movement(left_sensor, right_sensor);
}

int get_ultrasound_reading()
{
    digitalWrite(trig_pin, LOW);
    delayMicroseconds(2);
    digitalWrite(trig_pin, HIGH);
    delayMicroseconds(10);

    digitalWrite(trig_pin, LOW);
    ultrasound_duration = pulseIn(echo_pin, HIGH);

    ultrasound_distance = (ultrasound_duration * 0.034 / 2) + 1;
    Serial.println(ultrasound_distance);
}

float calibrate_colour_sensor()
{
    float total_value = 0;

    for (int t = 0; t < 100; t++)
    {
        if (analogRead(A0) < 1000)
        {
            total_value = abs(analogRead(colour_sensor_pin)) + total_value;
        }
        else
        {
            t--;
        }
    }
    return (total_value / 100);
}

void blockColour()
{
    int v = analogRead(colour_sensor_pin);

    if (v < colour_sensor_reference - 100)
    {
        digitalWrite(redLED, HIGH);
        digitalWrite(blueLED, LOW);
        isBlockRed = true;
        Serial.println("RED");
    }
    else
    {
        digitalWrite(blueLED, HIGH);
        digitalWrite(redLED, LOW);
        isBlockRed = false;
        Serial.println("BLUE");
    }
}

void loop()
{
    read_line_sensors();

    if ((line_follower_running) && (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)))
    {
        choose_movement(left_sensor, right_sensor);
        if (enough_points_crossed)
        {
            servo_lift.write(servo_lift_pos_down);
            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);

            points_crossed = 0;
            calibrate = true;
            enough_points_crossed = false;
        }
    }
    else if (calibrate)
    {
        if (colour_sensor_reference == 0)
        {
            rightMotor->setSpeed(0);
            leftMotor->setSpeed(0);
            servo_claw.write(servo_claw_pos_closed);
            servo_lift.write(servo_lift_pos_down);
            delay(500);
            colour_sensor_reference = calibrate_colour_sensor();
            delay(500);
            servo_claw.write(servo_claw_pos_open);
            servo_lift.write(servo_lift_pos_up);
            delay(500);
            go_slow = true;
            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);
        }
        if (first_run)
        {
            final_approach = true;
        }
        else
        {
            approach_sweep = true;
        }
        calibrate = false;
    }
    else if (final_approach)
    {
        line_follower_running = true;
        get_ultrasound_reading();
        if (ultrasound_distance < 13)
        {
            Serial.println("Distance is less than 12");
            rightMotor->setSpeed(0);
            leftMotor->setSpeed(0);
            colour_identification = true;
            final_approach = false;
        }

        delay(time_interval - 12);
        return;
    }
    else if (approach_sweep)
    {
        left_encoder_count = 0;
        right_encoder_count = 0;
        while (distanceTravelled() <= (35.0))
        {
            Serial.println(distanceTravelled());
            read_line_sensors();
            choose_movement(left_sensor, right_sensor);
            delay(time_interval);
        }
        sweep_for_block = true;
        line_follower_running = false;
        approach_sweep = false;
    }
    else if (sweep_for_block)
    {
        rightMotor->run(BACKWARD);
        leftMotor->setSpeed(fwd_speed - 50);
        rightMotor->setSpeed(fwd_speed - 50);
        int sweep_counter = 0;
        while (sweep_counter < 1250 / time_interval)
        {
            get_ultrasound_reading();
            if (ultrasound_distance > min_distance_to_block && ultrasound_distance < max_distance_to_block)
            {
                delay(100);
                rightMotor->setSpeed(0);
                leftMotor->setSpeed(0);
                rightMotor->run(FORWARD);
                block_found = true;
                sweep_for_block = false;
                return;
            }
            sweep_counter++;
            delay(time_interval);
        }
        sweep_counter = 0;
        rightMotor->run(FORWARD);
        leftMotor->run(BACKWARD);
        while (sweep_counter < 2200 / time_interval)
        {
            get_ultrasound_reading();
            if (ultrasound_distance > min_distance_to_block && ultrasound_distance < max_distance_to_block)
            {
                delay(100);
                rightMotor->setSpeed(0);
                leftMotor->setSpeed(0);
                leftMotor->run(FORWARD);
                block_found = true;
                sweep_for_block = false;
                return;
            }
            sweep_counter++;
            delay(time_interval);
        }

        sweep_counter = 0;
        leftMotor->run(FORWARD);
        rightMotor->run(BACKWARD);

        while (sweep_counter < 1350 / time_interval)
        {
            get_ultrasound_reading();
            if (ultrasound_distance > min_distance_to_block && ultrasound_distance < max_distance_to_block)
            {
                delay(100);
                rightMotor->setSpeed(0);
                leftMotor->setSpeed(0);
                rightMotor->run(FORWARD);
                block_found = true;
                sweep_for_block = false;
                return;
            }
            sweep_counter++;
            delay(time_interval);
        }
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        delay(10000);
    }
    else if (block_found)
    {
        while (block_found)
        {
            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);
            line_follower_running = true;
            get_ultrasound_reading();
            if (ultrasound_distance == 12)
            {
                Serial.println("Distance is less than 12");
                rightMotor->run(FORWARD);
                leftMotor->run(FORWARD);
                rightMotor->setSpeed(0);
                leftMotor->setSpeed(0);
                Serial.println("DISTANCE IS 12");
                delay(2000);
                colour_identification = true;
                line_follower_running = false;
                block_found = false;
                continue;
            }
            if (ultrasound_distance > 12)
            {
                rightMotor->run(FORWARD);
                leftMotor->run(FORWARD);
                Serial.println("FORWARDS");
            }
            if (ultrasound_distance < 12)
            {
                rightMotor->run(BACKWARD);
                leftMotor->run(BACKWARD);
                Serial.println("GOING BACK");
            }

            delay(time_interval - 12);
        }
        return;
    }
    else if (colour_identification)
    {
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        servo_claw.write(servo_claw_pos_closed);
        servo_lift.write(servo_lift_pos_down);
        delay(500);
        blockColour();
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        delay(5000);
        block_snatch = true;
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        colour_identification = false;
    }

    else if (block_snatch)
    {
        go_slow = false;
        servo_lift.write(servo_lift_pos_down);
        delay(300);
        servo_claw.write(servo_claw_pos_open);
        delay(300);
        leftMotor->setSpeed(fwd_speed);
        rightMotor->setSpeed(fwd_speed);
        delay(1500);
        move_forwards_pickup();
        delay(2000);
        rightMotor->setSpeed(fwd_speed);
        leftMotor->setSpeed(fwd_speed);
        left_encoder_count = 0;
        right_encoder_count = 0;
        line_follower_running_return = true;
        line_follower_running = true;
        block_snatch = false;
    }

    else if (line_follower_running_return && (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)))
    {

        choose_movement(left_sensor, right_sensor);
    }

    else if (return_encoder)
    {
        line_follower_running_return = false;
        line_follower_running = true;
        left_encoder_count = 0;
        right_encoder_count = 0;
        Serial.println("Approaching Dropoff");
        //  We take away 2 for calibration purposes
        while (distanceTravelled() <= (distance_to_90_deg_turn - 4.0))
        {
            Serial.println(distanceTravelled());
            read_line_sensors();
            choose_movement(left_sensor, right_sensor);
            delay(time_interval);
        }
        leftMotor->setSpeed(0);
        rightMotor->setSpeed(0);
        delay(2000);
        // pivot(90.0, false);
        Serial.println("In position for turn");
        if (isBlockRed)
        {
            halfTurn(6, isBlockRed); // !isBlockRed turns it in the right direction depending on block colour
        }
        else
        {
            halfTurn(4, isBlockRed);
        }
        line_follower_running = false;
        delay(1000);

        approach_block = true;
        line_follower_running = false;
        is_approaching_dropoff = false;
        return_encoder = false;
    }

    else if (approach_block)
    {
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        read_line_sensors();
        choose_movement(left_sensor, right_sensor);
        if (sensors_high)
        {
            Serial.println("Dropoff line found");
            sensors_high = false;
            rightMotor->setSpeed(0);
            leftMotor->setSpeed(0);
            delay(2000);
            leftMotor->run(BACKWARD);
            rightMotor->run(BACKWARD);

            rightMotor->setSpeed(turn_speed_slow);
            leftMotor->setSpeed(turn_speed_slow);
            left_encoder_count = 0;
            right_encoder_count = 0;
            go_back_dropoff = true;
            approach_block = false;
        }
    }

    else if (go_back_dropoff)
    {
        Serial.println("Going back");
        line_follower_running = false;
        while (distanceTravelled() <= distance_back_dropoff - 2.5)
        {
            delay(time_interval);
        }
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        if (isBlockRed)
        {
            leftMotor->run(FORWARD);
        }
        else
        {
            rightMotor->run(BACKWARD);
        }
        if (isBlockRed)
        {
            delay(150);
        }
        else
        {
            delay(950);
        }

        leftMotor->run(BACKWARD);
        rightMotor->run(BACKWARD);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        Serial.println("Dropping off");
        delay(2000);
        servo_lift.write(servo_lift_pos_down);
        delay(500);
        servo_claw.write(servo_claw_pos_open);
        delay(500);
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        return_to_line = true;
        go_back_dropoff = false;
    }

    else if (return_to_line)
    {
        Serial.println("Back to line");
        while (distanceTravelled() <= distance_to_line)
        {
            Serial.println(distanceTravelled());
            delay(time_interval);
        }
        servo_claw.write(servo_claw_pos_closed);
        delay(500);
        servo_lift.write(servo_lift_pos_up);
        delay(1000);
        Serial.println("Turning");
        // The parameter 6 below works for the red table
        halfTurn(6, !isBlockRed);
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);

        Serial.println("Done");
        while (!sensors_high)
        {
            read_line_sensors();
            choose_movement(left_sensor, right_sensor);
            delay(50);
        }

        left_encoder_count = 0;
        right_encoder_count = 0;
        rightMotor->setSpeed(turn_speed_slow);
        leftMotor->setSpeed(turn_speed_slow);
        rightMotor->run(FORWARD);
        leftMotor->run(FORWARD);
        while (distanceTravelled() <= 21.5)
        {
            Serial.println(distanceTravelled());
            delay(time_interval);
        }
        rightMotor->setSpeed(0);
        leftMotor->setSpeed(0);
        first_run = false;
        return_to_line = false;
        start_code = false;
    }

    while (!start_code)
    {
        Serial.println("Idle");
        delay(100);
    }

    delay(time_interval);
}
