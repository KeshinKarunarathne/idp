#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include "Ticker.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object

const int fwd_speed = 200;       // Default forward speed
const int turn_speed_slow = 130; // Speed of slower motor during a turn
const int turn_speed_fast = 230;  // Speed of faster motor during a turn
const int pivot_speed = 200; // Speed of both motors while pivoting
int time_interval = 50;

// LINE FOLLOWERS
const int right_sensor_pin = 4;  // Right line sensor
const int left_sensor_pin = 5;   // Left line sensor
bool left_sensor;                // Left line follower sensor state
bool old_left_sensor;            // Keeps track of old value for comparison
bool right_sensor;               // Right line follower sensor state
bool old_right_sensor;           // Keeps track of old value for comparison
int lines_passed = 0;            // Tracks number of horizontal stripes passed
bool on_horizontal_line = false; // State that determines if it is currently above a stripe
bool sensors_high = false;
int points_crossed = 0;

// ENCODERS
const int left_encoder_pin = 3; // Encoder pin (has to be pin 2 or 3 for Arduino Uno)
const int right_encoder_pin = 2;
int left_encoder_count = 0; // Number of times the left photointerrupter changes state
int right_encoder_count = 0; // Number of times the right photointerrupter changes state
float average_encoder_count = 0.5*(left_encoder_count+right_encoder_count);

const float encoder_wheel_radius = 4.0; // Radius of encoder wheel
const float robot_wheel_radius = 5.25; // Radius of robot wheels
const float distance_between_wheels = 25.7; // Distance between robot wheels
const float pivot_circle_radius = distance_between_wheels/2; // Radius of pivot circle
float pivot_arc; // The arc through which each wheel should turn in pivotAngle()
int target_encoder_count = 0; // Encoder count required to turn a certain pivot angle
const int encoder_holes = 36; // Number of holes in the encoder wheel
float distance_travelled;
float distance_from_line_sensor_to_wheels = 15.5;
float distance_from_marker_to_drop_off_line = 18.0;
float distance_to_90_deg_turn = distance_from_line_sensor_to_wheels + distance_from_marker_to_drop_off_line;
const float distance_back_dropoff = 9.0;
const float distance_to_line = 27.5;


bool line_follower_running = true;
bool is_pivot = false; // This is used to run pivot only once
bool is_moving_forward = true; // This is required to use the encoders for measuring distance
bool is_approaching_dropoff = true;
bool half_turn_activated = false;
bool approach_block = false;
bool go_back_dropoff = false;
bool return_to_line = false;

void setup() {
  AFMS.begin(); // Connecting to the controller

  Serial.begin(9600);

  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);

  old_left_sensor = true;
  old_right_sensor = true;

  // Speed ranges from 0(stopped) to 255(full speed)
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(120);
  rightMotor->setSpeed(120);
  
  pinMode(left_encoder_pin, INPUT);
  pinMode(right_encoder_pin, INPUT);

  // The digitalPinToInterrupt() may be required if interrupt doesn't work
  attachInterrupt(digitalPinToInterrupt(left_encoder_pin), left_encoder_counter, CHANGE);
  attachInterrupt(digitalPinToInterrupt(right_encoder_pin), right_encoder_counter, CHANGE);
}

void left_encoder_counter() {
//  Serial.println(left_encoder_count);
//  if (left_encoder_count < target_encoder_count) {
//     left_encoder_count++;  
//  }
  left_encoder_count++;
}

void right_encoder_counter() {
 // Serial.println(right_encoder_count);
//  if (right_encoder_count < target_encoder_count) {
//     right_encoder_count++;
  right_encoder_count++;
}

void pivot(float pivot_angle, bool isClockwise) {
  pivot_arc = (pivot_angle/360.0)*2*PI*pivot_circle_radius;
  target_encoder_count = int(encoder_holes*2*(pivot_arc/(2*PI*robot_wheel_radius)));
  Serial.println(target_encoder_count);

  if (isClockwise) {
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
  }
  else {
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
  }
  
  left_encoder_count=0;
  right_encoder_count=0;

  leftMotor->setSpeed(pivot_speed);
  rightMotor->setSpeed(pivot_speed);

// Following loop had a slight systematic offset
//  while(left_encoder_count < target_encoder_count || right_encoder_count < target_encoder_count){
//    Serial.println(left_encoder_count);
// }

  // Added 1.5 below just for calibration
  while(int(0.5*(left_encoder_count + right_encoder_count+1)) < target_encoder_count) {
    Serial.println(left_encoder_count);
  }
  
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);

  is_pivot = false;
  Serial.println("End of Pivot");
}

void halfTurn(int number)
{
  // This method turns the robot by 180 degrees after grabbing a block
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(turn_speed_slow+50);
  rightMotor->setSpeed(turn_speed_slow+50);
  

  int turn_line_crossings = 0;
  while (turn_line_crossings < number)
  {
    read_line_sensors();
    if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor))
    {
      turn_line_crossings++;
      Serial.println(turn_line_crossings);
    }
    old_left_sensor = left_sensor;
    old_right_sensor = right_sensor;
    delay(time_interval);
  }
  Serial.println("Turn over");
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
}


float distanceTravelled(){
  //float average_encoder_count = 0.5*(left_encoder_count+right_encoder_count);
  distance_travelled = 2*PI*robot_wheel_radius*(0.5*(left_encoder_count+right_encoder_count)/(encoder_holes*2));
  return distance_travelled;
}


void read_line_sensors()
{
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
}

// Turn the robot to the right while still moving
void rightTurn()
{
  //Serial.println("Turning Right");
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
}
// Turn the robot to the left while still moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
}
// Make the robot move in the direction its pointing
void goStraight()
{
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);
}

void choose_movement(int left, int right)
{

  if (left == false && right == false)
  {
    goStraight();
    if (sensors_high == true)
    {
      sensors_high = false;
      points_crossed++;
      //Serial.println("Point crossed");
    }
  }
  else if (left == true && right == false)
  {
    leftTurn();
  }
  else if (left == false && right == true)
  {
    rightTurn();
  }
  else if (left == true && right == true)
  {
    goStraight();
    sensors_high = true;
  }
  old_left_sensor = left;
  old_right_sensor = right;
  
}

void loop() {
  read_line_sensors();


  if((!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)) && points_crossed < 1 && line_follower_running){
    read_line_sensors();
    choose_movement(left_sensor, right_sensor);
  }
  else if (points_crossed >= 1 && is_approaching_dropoff){
    left_encoder_count=0;
    right_encoder_count=0;
    //Serial.println("Approaching Dropoff");
    // We take away 2 for calibration purposes
    while(distanceTravelled() <= (distance_to_90_deg_turn-2.0)){
      read_line_sensors();
      choose_movement(left_sensor, right_sensor);
      delay(time_interval);
    }
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);
    delay(2000);
    //pivot(90.0, false);
    halfTurn(6);
    line_follower_running = false;
    delay(2000);
    approach_block = true;
    line_follower_running = false;
    is_approaching_dropoff = false;
  }
  /*
  if (half_turn_activated){
    delay(5000);
    left_encoder_count=0;
    right_encoder_count=0;
    leftMotor->setSpeed(fwd_speed);
    rightMotor->setSpeed(fwd_speed);
    while(distanceTravelled() <= (20-4.0)){
      //Serial.println(distanceTravelled());
      read_line_sensors();
      choose_movement(left_sensor, right_sensor);
    }
    leftMotor->setSpeed(0);
    rightMotor->setSpeed(0);

    half_turn_activated = false;
  }
  */

  else if (approach_block) {
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_slow);
    read_line_sensors();
    choose_movement(left_sensor, right_sensor);
    if (sensors_high) {
      sensors_high = false;
      rightMotor->setSpeed(0);
      leftMotor->setSpeed(0);
      delay(2000);
      leftMotor->run(BACKWARD);
      rightMotor->run(BACKWARD);

      rightMotor->setSpeed(turn_speed_slow);
      leftMotor->setSpeed(turn_speed_slow);
      left_encoder_count = 0;
      right_encoder_count = 0;
      go_back_dropoff = true;
      approach_block = false;
      
    }

    delay(time_interval);
  }

  else if (go_back_dropoff) {
    line_follower_running = false;
    while(distanceTravelled() <= distance_back_dropoff){
      delay(time_interval);
    }
    rightMotor->setSpeed(0);
    leftMotor->setSpeed(0);
    delay(2000);
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_slow);
    return_to_line = true;
    go_back_dropoff= false;
  }

  else if (return_to_line) {

    while(distanceTravelled() <= distance_to_line){
      delay(time_interval);
    }
    delay(1000);
    halfTurn(6);
    rightMotor->setSpeed(0);
    leftMotor->setSpeed(0);
    delay(15000);
    

    
    
  }

}
