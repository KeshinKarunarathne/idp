#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object

const int fwd_speed = 240;       // Default forward speed
const int turn_speed_slow = 120; // Speed of slower motor during a turn
const int turn_speed_fast = 240;  // Speed of faster motor

bool left_sensor; // Left line follower sensor
bool old_left_sensor;
bool right_sensor; // Right line follower sensor
bool old_right_sensor;
const int right_sensor_pin = 2; 
const int left_sensor_pin = 4;
const int sensors_power = 7;
bool sensors_high = false;
int points_crossed = 0;

void setup()
{
  AFMS.begin(); // Connecting to the controller

  Serial.begin(9600); // For debugging

  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);
  
  pinMode(sensors_power, OUTPUT);

  // Speed ranges from 0(stopped) to 255(full speed)
  leftMotor->run(BACKWARD);
  rightMotor->run(BACKWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);

  old_left_sensor = true;
  old_right_sensor = true;
}


// Read sensor values for the line follower sensors
void read_line_sensors()
{
  digitalWrite(sensors_power, HIGH);
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
//  Serial.print(left_sensor)
//  Serial.println(right_sensor)
}

// Turn the robot to the right while still moving
void rightTurn()
{
  Serial.println("Turning Right");
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
}
// Turn the robot to the left while still moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
}
// Make the robot move in the direction its pointing
void goStraight()
{
  Serial.println("Going forward");
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);
}

void choose_movement(int left, int right)
{
  if (left == false && right == false)
  {
    goStraight();
    if (sensors_high == true) {
      sensors_high = false;
      points_crossed++;
    }
  }
  else if (left == true && right == false)
  {
    leftTurn();
  }
  else if (left == false && right == true)
  {
    rightTurn();
  }
  else if (left == true && right == true)
  {
    goStraight();
    sensors_high = true;
  }
  old_left_sensor = left;
  old_right_sensor = right;
}

void loop(){
  
  //Fix this in void loop()

  // get sensor data
  read_line_sensors();
  
  
  // if sensors changed, change speed of motors
  if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor) && (points_crossed < 4))
  {
    choose_movement(left_sensor, right_sensor);
  }
  delay(50);
}
