#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>
#include <Ticker.h>


// ADAFRUIT MOTORSHIELD
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object
const int fwd_speed = 200;       // Default forward speed
const int turn_speed_slow = 180; // Speed of slower motor during a turn
const int turn_speed_fast = 220; // Speed of faster motor during a turn


// START BUTTON
const int start_button; // Declares the pin of the start_button


// SERVO
Servo servo_claw;       // Declaring first servo object
Servo servo_lift;       // Declaring second servo object
Servo servo_ultrasound; // Declaring thrid servo object
const int servo_claw_pin = 12;  // Servo controlling claw
const int servo_ultrasound_pin = 13;  // Servo controlling ultrasound
const int servo_lift_pin;  // Servo controlling pitch of claw
const int servo_ultrasound_approach_angle = 125; // The angle to detect the block on first approach
int servo_ultrasound_pos;  // Position of the servo carrying the ultrasound sensors
int servo_claw_pos=85;  // Position of the servo powering the claw
int servo_lift_pos;  // Position of the claw pitch sensors


// ULTRASONIC SENSOR
const int ultrasound_trig_pin; // Declaring the ultrasound trig pin
const int ultrasound_echo_pin; // Declaring the ultrasound echo pin
long echo_duration; // Duration of echo
int distance_to_block; // Distance to block
const int stop_distance; // Distance from the block to the robot at which the robot should stop
const int drop_off_offset; // Distance the robot must move back when dropping off


// LEDS
const int ambLED;  // Declaring amber LED pin
const int redLED;  // Declaring red LED pin
const int blueLED; // Declaring blue LED pin
int amber_led_state = HIGH; // State of amber LED
const int test_button = 6;


// LINE FOLLOWERS
const int right_sensor_pin = 10; // Right line sensor
const int left_sensor_pin = 11; // Left line sensor
bool left_sensor; // Left line follower sensor state
bool old_left_sensor;  // Keeps track of old value for comparison
bool right_sensor; // Right line follower sensor state
bool old_right_sensor; // Keeps track of old value for comparison
int lines_passed = 0; // Tracks number of horizontal stripes passed
bool on_horizontal_line = false; // State that determines if it is currently above a stripe


// ENCODER
const int encoder_pin; // Encoder pin
const int encoder_holes = 36; // Number of holes in the encoder wheel
int encoder_count = 0; // Number of times the photointerrupter change state
float distance_from_start = 0.0; // Distance from start, monitored by the rotary encoder
float encoder_wheel_radius; // Radius of encoder wheel


// Preparing ticker for the amber LED
void changeState();
Ticker ambLED_Ticker(changeState, 250);

const long half_turn_duration = 3500; // Duration of 180-degree turn in ms


void setup()
{
  AFMS.begin(); // Connecting to the controller

  pinMode(start_button, OUTPUT);

  Serial.begin(9600); // For debugging

  pinMode(ambLED, OUTPUT);
  pinMode(redLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
  pinMode(start_button, INPUT);
  ambLED_Ticker.start() // Starts the ticker assigned to the amber LED

  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);

  pinMode(ultrasound_trig_pin,OUTPUT);
  pinMode(ultrasound_echo_pin,INPUT);

  servo_claw.attach(servo_claw_pin);
  servo_claw.write(servo_claw_pos);

  servo_ultrasound.attach(servo_ultrasound_pin);
  servo_lift.attach(servo_lift_pin);

  //pinMode(encoder_pin, INPUT);
  //attachInterrupt(digitalPinToInterrupt(encoder_pin), encoder_counter, CHANGE);

  // Speed ranges from 0(stopped) to 255(full speed)
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);

  amber_led_state = LOW;

  old_left_sensor = true;
  old_right_sensor = true;
}


void changeState() {
  amber_led_state = !amber_led_state;
  digitalWrite(ambLED, amber_led_state);
}


// Read sensor values for the line follower sensors
void read_line_sensors()
{
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
}

// Turn the robot to the right while still moving
void rightTurn()
{
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
}
// Turn the robot to the left while still moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
}
// Make the robot move in the direction its pointing
void goStraight()
{
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);

  // if we are no longer on the line, increase the line counter
  if (on_horizontal_line == true)
  {
    lines_passed++;
    on_horizontal_line = false;
  }
}

void robot_stop() {
  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);
}

void find_block()
{
  // find the location of a block within the collecting area
  digitalWrite(ultrasound_trig_pin,LOW);
  delayMicroseconds(2);
  digitalWrite(ultrasound_trig_pin,HIGH);
  delayMicroseconds(10);
  digitalWrite(ultrasound_trig_pin,LOW);
  
  echo_duration=pulseIn(ultrasound_echo_pin,HIGH);
  distance_to_block=(echo_duration*0.034/2) + 1;
}

void identifyBlock()
{
  // this method uses the colour sensor to determine the block's colour
}

void grabBlock()
{
  // this method turns the servos in steps of one to grab and lift the block
    servo_claw.write(93);   
    delay(15);

    servo_lift.write(servo_lift_pos);
    delay(15);
}

void mark_reference()
{
  // if three lines are passed, we are on our way to grab a block: call find_block()
  if (lines_passed >= 3)
  {
    lines_passed = 0; // Reset number of lines passed 
    find_block();
  }
  // mark that we are on a horizontal line
  on_horizontal_line = true;
}

void choose_movement(int left, int right)
{
  if (left == false && right == false)
  {
    goStraight();
  }
  else if (left == true && right == false)
  {
    leftTurn();
  }
  else if (left == false && right == true)
  {
    rightTurn();
  }
  else if (left == true && right == true)
  {
    mark_reference();
  }
  old_left_sensor = left;
  old_right_sensor = right;
}

void set_up_first_approach() {
  
}

void encoder_counter() {
  encoder_count++;
}

float distance(){
  distance_from_start = 2*PI*encoder_wheel_radius*(encoder_count/encoder_holes);
  return distance_from_start;
}

void halfTurn() {
  //This method turns the robot by 180 degrees after grabbing a block
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);

  //Executes half turn using set time duration
  const unsigned long start_time = millis();

  while(millis() - start_time <= half_turn_duration) {
    rightMotor->setSpeed(fwd_speed);
    leftMotor->setSpeed(fwd_speed);
   }

  rightMotor->setSpeed(0));
  leftMotor->setSpeed(0);
}

void grab() {
  servo_claw.write(75);
  goStraight();
  delay(1000);
  robot_stop();
  servo_claw.write(100);
}


void loop()
{

  // The amber LED flashes (f = 2 Hz) while the robot moves

  ambLED_Ticker.update();

  /* 
    //Alternative to Ticker:
    light_counter = 0;
    if (amber_led_state == HIGH) {
      amber_led_state = LOW;
    }
    else {
      amber_led_state = HIGH;
    }
    digitalWrite(ambLED, amber_led_state);
  }
  */
`

  // get sensor data
  read_line_sensors();

  // if sensors changed, change speed of motors
  if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor))
  {
    choose_movement(left_sensor, right_sensor);
  }
  if (digitalRead(test_button) == HIGH) {
    halfTurn(1000);
  }
  delay(100);
}
