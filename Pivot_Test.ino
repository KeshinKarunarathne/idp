#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include "Ticker.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object

const int fwd_speed = 200;       // Default forward speed
const int turn_speed_slow = 130; // Speed of slower motor during a turn
const int turn_speed_fast = 230;  // Speed of faster motor during a turn
const int pivot_speed = 200; // Speed of both motors while pivoting


// ENCODERS
const int left_encoder_pin = 3; // Encoder pin (has to be pin 2 or 3 for Arduino Uno)
const int right_encoder_pin = 2;
int left_encoder_count = 0; // Number of times the left photointerrupter changes state
int right_encoder_count = 0; // Number of times the right photointerrupter changes state

const float encoder_wheel_radius = 4.0; // Radius of encoder wheel
const float robot_wheel_radius = 5.25; // Radius of robot wheels
const float distance_between_wheels = 25.7; // Distance between robot wheels
const float pivot_circle_radius = distance_between_wheels/2; // Radius of pivot circle
float pivot_arc; // The arc through which each wheel should turn in pivotAngle()
int target_encoder_count = 0; // Encoder count required to turn a certain pivot angle
const int encoder_holes = 36; // Number of holes in the encoder wheel
float distance_travelled;

bool is_pivot = true;

void setup() {
  AFMS.begin(); // Connecting to the controller

  // Speed ranges from 0(stopped) to 255(full speed)
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(120);
  rightMotor->setSpeed(120);
  
  Serial.begin(9600);
  
  pinMode(left_encoder_pin, INPUT);
  pinMode(right_encoder_pin, INPUT);

  // The digitalPinToInterrupt() may / may not be required if interrupt doesn't work
  attachInterrupt(digitalPinToInterrupt(left_encoder_pin), left_encoder_counter, CHANGE);
  attachInterrupt(digitalPinToInterrupt(right_encoder_pin), right_encoder_counter, CHANGE);
}

void left_encoder_counter() {
  //Serial.println(left_encoder_count);
  if (left_encoder_count < target_encoder_count) {
     left_encoder_count++;  
  }
}

void right_encoder_counter() {
 // Serial.println(right_encoder_count);
  if (right_encoder_count < target_encoder_count) {
     right_encoder_count++;
  }
}

void pivot(float pivot_angle, bool isClockwise) {
  pivot_arc = (pivot_angle/360.0)*2*PI*pivot_circle_radius;
  target_encoder_count = int(encoder_holes*2*(pivot_arc/(2*PI*robot_wheel_radius)));
  Serial.println(target_encoder_count);

  if (isClockwise) {
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
  }
  else {
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
  }
  
  left_encoder_count=0;
  right_encoder_count=0;

  leftMotor->setSpeed(pivot_speed);
  rightMotor->setSpeed(pivot_speed);

// Following loop had a slight systematic offset
//  while(left_encoder_count < target_encoder_count || right_encoder_count < target_encoder_count){
//    Serial.println(left_encoder_count);
// }

  // Added 1 below just for calibration
  while(int(0.5*(left_encoder_count + right_encoder_count + 1)) < target_encoder_count) {
    Serial.println(left_encoder_count);
  }
  
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);

  is_pivot = false;
  Serial.println("Pivot Finished");
}

void loop() {
// Testing pivot()
  if (is_pivot) { 
    pivot(90.0, false);
  }   
}
