#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

// ADAFRUIT MOTORSHIELD
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object
const int fwd_speed = 200;                          // Default forward speed
const int turn_speed_slow = 140;                    // Speed of slower motor during a turn
const int turn_speed_fast = 220;                    // Speed of faster motor during a turn
const int pivot_speed = 160;                        // Speed of both motors while pivoting

// SERVOS
Servo servo_claw;              // Declaring first servo object
Servo servo_lift;              // Declaring second servo object
const int servo_claw_pin = 9;  // Servo controlling claw
const int servo_lift_pin = 10; // Servo controlling pitch of claw
int servo_claw_pos_open = 100; // Position of the servo powering the claw
int servo_claw_pos_closed = 145;
int servo_lift_pos_up = 60; // Position of the claw pitch servo
int servo_lift_pos_down = 160;
int approach_count = 0;

// COLOUR SENSOR AND LEDs
int colour_sensor_pin = A0;      // Declaring colour sensor pin
const int redLED = 6;            // Declaring red LED pin
const int blueLED = 7;           // Declaring blue LED pin
int colour_sensor_reference = 0; // Reference obtained by calibrating the colour sensor
bool isBlockRed;                 // Boolean set to true if the block is red, otherwise false

// ULTRASONIC SENSOR
const int ultrasound_trig_pin = 11; // Declaring the ultrasound trig pin
const int ultrasound_echo_pin = 12; // Declaring the ultrasound echo pin
long echo_duration;                 // Duration of echo
long ultrasound_distance;
const float max_distance_to_block=25.0;
const float min_distance_to_block=5.0;
float distance_to_block = max_distance_to_block; // Distance to block populated in ultrasonicSweep() and used in findBlock()
const float first_sweep_angle = 21.80;
const float second_sweep_angle = 49.27;

// LINE FOLLOWERS
const int right_sensor_pin = 4;  // Right line sensor
const int left_sensor_pin = 5;   // Left line sensor
bool left_sensor;                // Left line follower sensor state
bool old_left_sensor;            // Keeps track of old value for comparison
bool right_sensor;               // Right line follower sensor state
bool old_right_sensor;           // Keeps track of old value for comparison
int lines_passed = 0;            // Tracks number of horizontal stripes passed
bool on_horizontal_line = false; // State that determines if it is currently above a stripe

// ENCODERS
const int left_encoder_pin = 3; // Encoder pin (has to be pin 2 or 3 for Arduino Uno)
const int right_encoder_pin = 2;
int left_encoder_count = 0;  // Number of times the left photointerrupter changes state
int right_encoder_count = 0; // Number of times the right photointerrupter changes state

const float encoder_wheel_radius = 4.0;     // Radius of encoder wheel
const float robot_wheel_radius = 5.25;      // Radius of robot wheels
const float distance_between_wheels = 25.7; // Distance between robot wheels
const float distance_from_line_sensor_to_wheels = 15.5;
const int encoder_holes = 36;                            // Number of holes in the encoder wheel
float pivot_circle_radius = distance_between_wheels / 2; // Radius of pivot circle
float pivot_arc;                                         // The arc through which each wheel should turn in pivotAngle()
int target_encoder_count;                                // Encoder count required to turn a certain pivot angle
int encoder_count_to_block = 0;
bool block_direction; // Whether the block lies to the left(false) or right(true) of the centreline
float distance_travelled;

const int time_interval = 50;
bool grab_block = false;

bool is_running = true;

void setup()
{
  AFMS.begin(); // Connecting to the controller

  Serial.begin(9600); // For debugging

  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);

  pinMode(ultrasound_trig_pin, OUTPUT);
  pinMode(ultrasound_echo_pin, INPUT);

  pinMode(left_encoder_pin, INPUT);
  pinMode(right_encoder_pin, INPUT);

  servo_claw.attach(servo_claw_pin);
  servo_claw.write(servo_claw_pos_open);
  servo_lift.attach(servo_lift_pin);
  servo_lift.write(servo_lift_pos_up);

  pinMode(colour_sensor_pin, INPUT);
  pinMode(redLED, OUTPUT);
  pinMode(blueLED, OUTPUT);

  // Speed ranges from 0(stopped) to 255(full speed)
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);

  // Remove digitalPinToInterrupt() if working with the Arduino Uno Wifi Rev 2
  attachInterrupt(digitalPinToInterrupt(left_encoder_pin), left_encoder_counter, CHANGE);
  attachInterrupt(digitalPinToInterrupt(right_encoder_pin), right_encoder_counter, CHANGE);

  old_left_sensor = true;
  old_right_sensor = true;
  delay(5000);
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);
}

// Read sensor values for the line follower sensors
void read_line_sensors()
{
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
}

// Turn the robot to the right while still moving
void rightTurn()
{
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
}
// Turn the robot to the left while still moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
}
// Make the robot move in the direction its pointing
void goStraight()
{
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);
}

void get_ultrasound_reading()
{
  // find the location of a block within the collecting area
  digitalWrite(ultrasound_trig_pin, LOW);
  delayMicroseconds(2);
  digitalWrite(ultrasound_trig_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(ultrasound_trig_pin, LOW);

  echo_duration = pulseIn(ultrasound_echo_pin, HIGH);
  ultrasound_distance = (echo_duration * 0.034 / 2) + 1;
}

void left_encoder_counter()
{
  left_encoder_count++;
  //  Serial.print("Left Encoder Count: ");
  //  Serial.println(left_encoder_count);
}

void right_encoder_counter()
{
  right_encoder_count++;
  //  Serial.print("Right Encoder Count: ");
  //  Serial.println(right_encoder_count);
}

void blockColour()
{
  int v = analogRead(colour_sensor_pin);

  if (v < colour_sensor_reference - 100)
  {
    digitalWrite(redLED, HIGH);
    digitalWrite(blueLED, LOW);
    isBlockRed = true;
  }
  else
  {
    digitalWrite(blueLED, HIGH);
    digitalWrite(redLED, LOW);
    isBlockRed = false;
  }
}

float calibrate_colour_sensor()
{
  float total_value = 0;

  for (int t = 0; t < 100; t++)
  {
    if (analogRead(A0) < 1000)
    {
      total_value = abs(analogRead(colour_sensor_pin)) + total_value;
    }
    else
    {
      t--;
    }
  }
  return (total_value / 100);
}

void pivot_using_angle(float pivot_angle, bool isClockwise)
{

  pivot_arc = (pivot_angle / 360.0) * 2 * PI * pivot_circle_radius;
  target_encoder_count = int(encoder_holes * 2 * (pivot_arc / (2 * PI * robot_wheel_radius)));
  Serial.println(target_encoder_count);

  if (isClockwise)
  {
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
  }
  else
  {
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
  }

  left_encoder_count = 0;
  right_encoder_count = 0;

  leftMotor->setSpeed(pivot_speed);
  rightMotor->setSpeed(pivot_speed);

  // Following loop had a slight systematic offset
  //  while(left_encoder_count < target_encoder_count || right_encoder_count < target_encoder_count){
  //    Serial.println(left_encoder_count);
  // }

  // Added 1 below just for calibration
  while (int(0.5 * (left_encoder_count + right_encoder_count + 1)) < target_encoder_count)
  {
    Serial.println(left_encoder_count);
  }

  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);

  // is_pivot = false;
  Serial.println("End of Pivot");
}

void pivot_using_target_count(int target_count, bool isClockwise)
{

  if (isClockwise)
  {
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
  }
  else
  {
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
  }

  left_encoder_count = 0;
  right_encoder_count = 0;

  leftMotor->setSpeed(pivot_speed);
  rightMotor->setSpeed(pivot_speed);

  // Added 1 below just for calibration
  while (int(0.5 * (left_encoder_count + right_encoder_count + 1)) < target_count)
  {
    Serial.println(left_encoder_count);
  }

  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
}

void ultrasonicSweep(float sweep_angle, bool isClockwise)
{

  pivot_arc = (sweep_angle / 360.0) * 2 * PI * pivot_circle_radius;
  target_encoder_count = int(encoder_holes * 2 * (pivot_arc / (2 * PI * robot_wheel_radius)));
  Serial.println(target_encoder_count);
  delay(1000);

  if (isClockwise)
  {
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
  }
  else
  {
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
  }

  left_encoder_count = 0;
  right_encoder_count = 0;

  leftMotor->setSpeed(pivot_speed);
  rightMotor->setSpeed(pivot_speed);

  // Added 1 in the two instances below just for calibration
  while (int(0.5 * (left_encoder_count + right_encoder_count + 1)) < target_encoder_count)
  {
    get_ultrasound_reading();
    if (ultrasound_distance >= min_distance_to_block && ultrasound_distance <= max_distance_to_block && ultrasound_distance < distance_to_block)
    {
      encoder_count_to_block = int(0.5 * (left_encoder_count + right_encoder_count + 1));
      distance_to_block = ultrasound_distance;
      grab_block = true;
      if (isClockwise == true)
      {
        block_direction = true;
      }
      else
      {
        block_direction = false;
      }
    }
    Serial.println(int(0.5 * (left_encoder_count + right_encoder_count + 1)));
    delay(500);
  }

  pivot_using_angle(sweep_angle, !isClockwise);
  Serial.println(distance_to_block);
  delay(50);
}

float distanceTravelled()
{
  // float average_encoder_count = 0.5*(left_encoder_count+right_encoder_count);
  distance_travelled = 2 * PI * robot_wheel_radius * (0.5 * (left_encoder_count + right_encoder_count) / (encoder_holes * 2));
  return distance_travelled;
}

void grab_block_and_return(float distance_to_move)
{
  // This method grabs the block after the sweep confirms its position and returns to the white line.
  pivot_using_target_count(encoder_count_to_block, block_direction);

  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);

  left_encoder_count = 0;
  right_encoder_count = 0;

  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);

  while (distanceTravelled() <= (distance_to_block - 2.0))
  {
    delay(time_interval);
  }

  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);

  // Rest of the code involves detecting the colour, picking up the block and returning to the line.

  /*
  */

  grab_block = false;
}

bool turn(int duration, bool clockwise)
{
  int count = 0;
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_slow);
  if (clockwise)
  {
    rightMotor->run(FORWARD);
    leftMotor->run(BACKWARD);
  }
  else
  {
    rightMotor->run(BACKWARD);
    leftMotor->run(FORWARD);
  }
  while (count < duration / time_interval)
  {
    get_ultrasound_reading();
    Serial.println(ultrasound_distance);
    count++;
    if (ultrasound_distance > min_distance_to_block && ultrasound_distance < max_distance_to_block)
    {
      Serial.print("Block found at a distance of: ");
      Serial.println(ultrasound_distance);
      return true;
    }
    delay(time_interval);
  }
  return false;
}

void loop()
{
  if (is_running && turn(1000, true))
  {
    is_running = false;
  }
  if (is_running && turn(2000, false))
  {
    is_running = false;
  }
  if (is_running && turn(1000, true))
  {
    is_running = false;
  }
  if (!is_running)
  {
    rightMotor->setSpeed(120);
    leftMotor->setSpeed(120);
    delay(500);
    rightMotor->setSpeed(0);
    leftMotor->setSpeed(0);
    delay(10000);
  }
}
