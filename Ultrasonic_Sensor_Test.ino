#define echoPin 2
#define trigPin 3

long duration;
int distance;

void setup(){
  Serial.begin(9600);
  pinMode(trigPin,OUTPUT);
  pinMode(echoPin,INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
 }
 
void loop(){
  digitalWrite(trigPin,LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);
  
 duration=pulseIn(echoPin,HIGH);
  distance=(duration*0.034/2) + 1;

  if (distance>7)
    {digitalWrite(LED_BUILTIN, HIGH);}
  else 
    {digitalWrite(LED_BUILTIN, LOW);}
    
  Serial.print("Distance : ");
  Serial.print(distance);
  Serial.println(" cm ");
  delay(1000);   
}
