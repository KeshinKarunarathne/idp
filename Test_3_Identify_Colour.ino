#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

// ADAFRUIT MOTORSHIELD
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object
const int fwd_speed = 240;                          // Default forward speed
const int turn_speed_slow = 120;                    // Speed of slower motor during a turn
const int turn_speed_fast = 240;                    // Speed of faster motor during a turn
const int time_interval = 50;

// SERVO
Servo servo_claw;              // Declaring first servo object
Servo servo_lift;              // Declaring second servo object
const int servo_claw_pin = 9;  // Servo controlling claw
const int servo_lift_pin = 10; // Servo controlling pitch of claw
int servo_claw_pos_open = 100; // Position of the servo powering the claw
int servo_claw_pos_closed = 145;
int servo_lift_pos_up = 60; // Position of the claw pitch servo
int servo_lift_pos_down = 160;
int approach_count = 0;

// COLOUR SENSOR AND LEDs
int colour_sensor_pin = A0;      // Declaring colour sensor pin
const int redLED = 7;            // Declaring red LED pin
const int blueLED = 8;           // Declaring blue LED pin
int colour_sensor_reference = 0; // Reference obtained by calibrating the colour sensor
bool isBlockRed;                 // Boolean set to true if the block is red, otherwise false

// LINE FOLLOWERS
const int right_sensor_pin = 4;  // Right line sensor
const int left_sensor_pin = 5;   // Left line sensor
bool left_sensor;                // Left line follower sensor state
bool old_left_sensor;            // Keeps track of old value for comparison
bool right_sensor;               // Right line follower sensor state
bool old_right_sensor;           // Keeps track of old value for comparison
int lines_passed = 0;            // Tracks number of horizontal stripes passed
bool on_horizontal_line = false; // State that determines if it is currently above a stripe

// ULTRASOUND
int echo_pin = 12;
int trig_pin = 11;
const int ultrasound_power_pin = 6;
long ultrasound_duration;
int ultrasound_distance;

bool sensors_high = false;
int points_crossed = 0;
int right_turn_detection_counter = 0;

const long half_turn_duration = 4500; // Duration of 180-degree turn in ms

bool line_follower_running = true;
bool line_follower_running_return = false;
bool calibrate = false;
bool block_pickup_running = false;
bool half_turn_running = false;
bool final_approach = false;
bool colour_identification = false;
bool block_snatch = false;
bool ninety_deg = false;
bool dropoff_line_follower = false;
bool move_and_drop = false;
bool back_to_line = false;
bool return_to_start = false;
bool start_reached = false;

bool go_slow = false;

bool enough_points_crossed = false;

int button_pin = 13;
bool start = false;

void setup()
{
  AFMS.begin(); // Connecting to the controller

  Serial.begin(9600); // For debugging
                      /*
                        pinMode(button_pin, INPUT);
                    
                        while (!start) {
                          if (digitalRead(button_pin) == HIGH) {
                            start = true;
                          }
                          delay(50);
                        }
                      */
  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);
  pinMode(ultrasound_power_pin, OUTPUT);

  pinMode(trig_pin, OUTPUT);
  pinMode(echo_pin, INPUT);

  servo_claw.attach(servo_claw_pin);
  servo_claw.write(servo_claw_pos_open);
  servo_lift.attach(servo_lift_pin);
  servo_lift.write(servo_lift_pos_up);

  pinMode(colour_sensor_pin, INPUT);
  pinMode(redLED, OUTPUT);
  pinMode(blueLED, OUTPUT);

  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);

  old_left_sensor = true;
  old_right_sensor = true;
  digitalWrite(ultrasound_power_pin, HIGH);
}

void read_line_sensors()
{
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
  right_turn_detection_counter = 0;
}

// Turn the robot to the right while still moving
void rightTurn()
{
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
  right_turn_detection_counter++;
}
// Turn the robot to the left while still moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
  right_turn_detection_counter++;
}
// Make the robot move in the direction its pointing
void goStraight()
{
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);
  right_turn_detection_counter = 0;
  // if we are no longer on the line, increase the line counter
  if (on_horizontal_line == true)
  {
    lines_passed++;
    on_horizontal_line = false;
  }
  if (go_slow)
  {
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_slow);
  }
}

void choose_movement(int left, int right)
{
  if (left == false && right == false)
  {
    goStraight();
    if (sensors_high == true)
    {
      sensors_high = false;
      points_crossed++;
      if ((!calibrate && !final_approach) && points_crossed >= 2)
      {
        enough_points_crossed = true;
      }
      if (!line_follower_running_return)
      {
        Serial.println("RETURN");
        line_follower_running = false;
        enough_points_crossed = true;
      }
    }
  }
  else if (left == true && right == false)
  {
    leftTurn();
  }
  else if (left == false && right == true)
  {
    rightTurn();
  }
  else if (left == true && right == true)
  {
    goStraight();
    sensors_high = true;
    if (dropoff_line_follower)
    {
      sensors_high = false;
      dropoff_line_follower = false;
      move_and_drop = true;
    }

    if (return_to_start)
    {
      start_reached = true;
      line_follower_running = false;
    }
  }
  old_left_sensor = left;
  old_right_sensor = right;
}

void halfTurn()
{
  // This method turns the robot by 180 degrees after grabbing a block
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);

  // Executes half turn using set time duration
  /*
  const unsigned long start_time = millis();

  while(millis() - start_time <= half_turn_duration) {
    rightMotor->setSpeed(fwd_speed);
    leftMotor->setSpeed(fwd_speed);
   }

  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);
  */

  int turn_line_crossings = 0;
  while (turn_line_crossings < 12)
  {
    read_line_sensors();
    if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor))
    {

      turn_line_crossings++;
    }
    old_left_sensor = left_sensor;
    old_right_sensor = right_sensor;
    delay(time_interval);
  }
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);
}

void move_forwards_pickup()
{
  servo_claw.write(servo_claw_pos_closed);
  delay(300);
  servo_lift.write(servo_lift_pos_up);

  rightMotor->run(BACKWARD);
  leftMotor->run(BACKWARD);
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);

  delay(1000);

  halfTurn();
}

int get_ultrasound_reading()
{
  digitalWrite(ultrasound_power_pin, HIGH);
  digitalWrite(trig_pin, LOW);
  delayMicroseconds(2);
  digitalWrite(trig_pin, HIGH);
  delayMicroseconds(10);

  digitalWrite(trig_pin, LOW);
  ultrasound_duration = pulseIn(echo_pin, HIGH);

  ultrasound_distance = (ultrasound_duration * 0.034 / 2) + 1;
}

float calibrate_colour_sensor()
{
  float total_value = 0;

  for (int t = 0; t < 100; t++)
  {
    if (analogRead(A0) < 1000)
    {
      total_value = abs(analogRead(colour_sensor_pin)) + total_value;
    }
    else
    {
      t--;
    }
  }
  return (total_value / 100);
}

void blockColour()
{
  int v = analogRead(colour_sensor_pin);

  if (v < colour_sensor_reference - 100)
  {
    digitalWrite(redLED, HIGH);
    digitalWrite(blueLED, LOW);
    isBlockRed = true;
  }
  else
  {
    digitalWrite(blueLED, HIGH);
    digitalWrite(redLED, LOW);
    isBlockRed = false;
  }
}

void ninety_deg_turn(int points_crossed, bool direction)
{
  // This method turns the robot by 180 degrees after grabbing a block
  if (direction)
  {
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
  }
  else
  {
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
  }
  int turn_line_crossings = 0;
  while (turn_line_crossings < points_crossed)
  {
    read_line_sensors();
    if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor))
    {
      turn_line_crossings++;
      Serial.println("Line detected");
    }
    old_left_sensor = left_sensor;
    old_right_sensor = right_sensor;
    delay(time_interval);
  }
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);
}

void loop()
{
  read_line_sensors();

  if ((line_follower_running) && (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)))
  {
    choose_movement(left_sensor, right_sensor);
    if (enough_points_crossed)
    {
      servo_lift.write(servo_lift_pos_down);
      rightMotor->setSpeed(turn_speed_slow);
      leftMotor->setSpeed(turn_speed_slow);
      points_crossed = 0;
      calibrate = true;
      enough_points_crossed = false;
    }
  }
  else if (calibrate)
  {
    if (colour_sensor_reference == 0)
    {
      rightMotor->setSpeed(0);
      leftMotor->setSpeed(0);
      servo_claw.write(servo_claw_pos_closed);
      servo_lift.write(servo_lift_pos_down);
      delay(500);
      colour_sensor_reference = calibrate_colour_sensor();
      delay(500);
      servo_claw.write(servo_claw_pos_open);
      servo_lift.write(servo_lift_pos_up);
      delay(500);
      go_slow = true;
      rightMotor->setSpeed(turn_speed_slow);
      leftMotor->setSpeed(turn_speed_slow);
    }
    final_approach = true;
    calibrate = false;
  }
  else if (final_approach)
  {
    get_ultrasound_reading();
    if (ultrasound_distance < 14)
    {
      Serial.println("Distance is less than 12");
      rightMotor->setSpeed(0);
      leftMotor->setSpeed(0);
      colour_identification = true;
      final_approach = false;
    }

    delay(time_interval - 12);
    return;
  }
  else if (colour_identification)
  {
    servo_claw.write(servo_claw_pos_closed);
    servo_lift.write(servo_lift_pos_down);
    delay(500);
    blockColour();
    delay(5000);
    block_snatch = true;
    colour_identification = false;
  }

  else if (block_snatch)
  {
    go_slow = false;
    servo_lift.write(servo_lift_pos_down);
    delay(300);
    servo_claw.write(servo_claw_pos_open);
    delay(300);
    leftMotor->setSpeed(fwd_speed);
    rightMotor->setSpeed(fwd_speed);
    delay(1500);
    move_forwards_pickup();
    line_follower_running_return = true;
    block_snatch = false;
  }

  else if (line_follower_running_return && (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)))
  {

    choose_movement(left_sensor, right_sensor);
    if (enough_points_crossed)
    {
      points_crossed = 0;
      ninety_deg = true;
      enough_points_crossed = false;
    }
  }

  else if (ninety_deg)
  {
    Serial.println("90deg");
    delay(500);
    ninety_deg_turn(4, true);
    line_follower_running = true;
    Serial.println("Line follower on");
    ninety_deg = false;
  }

  else if (move_and_drop)
  {
    Serial.println("Move and drop");
    line_follower_running = false;
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_slow);
    delay(300);
    rightMotor->setSpeed(0);
    leftMotor->setSpeed(0);
    Serial.println("Dropping");
    servo_lift.write(servo_lift_pos_down);
    delay(300);
    servo_claw.write(servo_claw_pos_open);
    delay(300);
    back_to_line = true;
    move_and_drop = false;
  }

  else if (back_to_line)
  {
    Serial.println("Back to the line");
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_slow);
    delay(100);
    servo_claw.write(servo_claw_pos_closed);
    delay(100);
    servo_lift.write(servo_lift_pos_up);
    delay(800);
    Serial.println("Turning");
    ninety_deg_turn(4, false);
    return_to_start = true;
    Serial.println("Following line back home");
    line_follower_running = true;
    back_to_line = false;
  }

  else if (start_reached)
  {
    Serial.println("Reached the start");
    rightMotor->setSpeed(turn_speed_slow);
    leftMotor->setSpeed(turn_speed_slow);
    delay(2000);
    rightMotor->setSpeed(0);
    leftMotor->setSpeed(0);
    start = false;
    start_reached = false;
  }

  while (!start)
  {
    Serial.println("Idle");
    delay(100);
  }

  delay(time_interval);
}
