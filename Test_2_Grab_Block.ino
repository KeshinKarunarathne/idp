#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

// ADAFRUIT MOTORSHIELD
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object
const int fwd_speed = 240;                          // Default forward speed
const int turn_speed_slow = 120;                    // Speed of slower motor during a turn
const int turn_speed_fast = 240;                    // Speed of faster motor during a turn
const int time_interval = 50;

// SERVO
Servo servo_claw;              // Declaring first servo object
Servo servo_lift;              // Declaring second servo object
const int servo_claw_pin = 9;  // Servo controlling claw
const int servo_lift_pin = 10; // Servo controlling pitch of claw
int servo_claw_pos_open = 100; // Position of the servo powering the claw
int servo_claw_pos_closed = 145;
int servo_lift_pos_up = 60; // Position of the claw pitch servo
int servo_lift_pos_down = 160;
bool final_approach = false;
int approach_count = 0;

// LINE FOLLOWERS
const int right_sensor_pin = 2;  // Right line sensor
const int left_sensor_pin = 4;   // Left line sensor
const int sensor_power_pin = 7;  // Power pin
bool left_sensor;                // Left line follower sensor state
bool old_left_sensor;            // Keeps track of old value for comparison
bool right_sensor;               // Right line follower sensor state
bool old_right_sensor;           // Keeps track of old value for comparison
int lines_passed = 0;            // Tracks number of horizontal stripes passed
bool on_horizontal_line = false; // State that determines if it is currently above a stripe

bool sensors_high = false;
int points_crossed = 0;

const long half_turn_duration = 4500; // Duration of 180-degree turn in ms

bool line_follower_running = true;
bool block_pickup_running = false;
bool half_turn_running = false;

void setup()
{
  AFMS.begin(); // Connecting to the controller

  Serial.begin(9600); // For debugging

  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);
  pinMode(sensor_power_pin, OUTPUT);

  servo_claw.attach(servo_claw_pin);
  servo_claw.write(servo_claw_pos_open);
  servo_lift.attach(servo_lift_pin);
  servo_lift.write(servo_lift_pos_up);

  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);

  old_left_sensor = true;
  old_right_sensor = true;
}

void read_line_sensors()
{
  digitalWrite(sensor_power_pin, HIGH);
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
}

// Turn the robot to the right while still moving
void rightTurn()
{
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
}
// Turn the robot to the left while still moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
}
// Make the robot move in the direction its pointing
void goStraight()
{
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);

  // if we are no longer on the line, increase the line counter
  if (on_horizontal_line == true)
  {
    lines_passed++;
    on_horizontal_line = false;
  }
}

void choose_movement(int left, int right)
{
  if (left == false && right == false)
  {
    goStraight();
    if (sensors_high == true)
    {
      sensors_high = false;
      points_crossed++;
      Serial.println("B");
      if (points_crossed >= 2)
      {
        servo_lift.write(servo_lift_pos_down);
        final_approach = true;
      }
    }
  }
  else if (left == true && right == false)
  {
    leftTurn();
  }
  else if (left == false && right == true)
  {
    rightTurn();
  }
  else if (left == true && right == true)
  {
    goStraight();
    Serial.println("A");
    sensors_high = true;
  }
  old_left_sensor = left;
  old_right_sensor = right;
}

void halfTurn()
{
  // This method turns the robot by 180 degrees after grabbing a block
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);

  // Executes half turn using set time duration
  /*
  const unsigned long start_time = millis();

  while(millis() - start_time <= half_turn_duration) {
    rightMotor->setSpeed(fwd_speed);
    leftMotor->setSpeed(fwd_speed);
   }

  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);
  */

  bool on_line_turn = false;
  int turn_line_crossings = 0;
  while (turn_line_crossings < 4)
  {
    read_line_sensors();
    if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor))
    {
      // Way 1, if used should set while (turn_line_crossings < 2)
      /*
      if (!on_line_turn && left_sensor != right_sensor)
      {
        on_line_turn = true;
        turn_line_crossings++;
        Serial.println("On a line")
      }
      else if (on_line_turn && left_sensor == false && right_sensor == false)
      {
        on_line_turn = false;
      }
      */
      // Way 2, if used should set while (turn_while_crossings < 4)
      turn_line_crossings++;
      Serial.println("Line detected");
    }
    old_left_sensor = left_sensor;
    old_right_sensor = right_sensor;
    delay(time_interval);
  }
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(fwd_speed);
  rightMotor->setSpeed(fwd_speed);
}

void move_forwards_pickup()
{
  servo_claw.write(servo_claw_pos_closed);
  delay(300);
  servo_lift.write(servo_lift_pos_up);

  rightMotor->run(BACKWARD);
  leftMotor->run(BACKWARD);
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);

  delay(1000);

  halfTurn();
  servo_claw.write(servo_claw_pos_open);
}

void loop()
{
  read_line_sensors();

  if ((line_follower_running) && (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)))
  {
    choose_movement(left_sensor, right_sensor);
  }
  if (final_approach)
  {
    if (++approach_count >= (1000 / time_interval))
    {
      line_follower_running = false;
      final_approach = false;
      move_forwards_pickup();
    }
  }
  if (block_pickup_running)
  {
    block_pickup_running = false;
    move_forwards_pickup();
  }

  delay(time_interval);
}
