#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object

const int fwd_speed = 200;       // Default forward speed
const int turn_speed_slow = 130; // Speed of slower motor during a turn
const int turn_speed_fast = 230;  // Speed of faster motor

// Declare pins for sensors and claw servos
const int right_sensor_pin = 2; 
const int left_sensor_pin = 4;
const int sensors_power = 7;

bool left_sensor; // Left line follower sensor
bool old_left_sensor;
bool right_sensor; // Right line follower sensor
bool old_right_sensor;

void setup()
{
  AFMS.begin(); // Connecting to the controller

  Serial.begin(9600); // For debugging

  pinMode(right_sensor_pin, INPUT);
  pinMode(left_sensor_pin, INPUT);
  
  pinMode(sensors_power, OUTPUT);

  // Speed ranges from 0(stopped) to 255(full speed)
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  leftMotor->setSpeed(100);
  rightMotor->setSpeed(100);

  old_left_sensor = true;
  old_right_sensor = true;
}

// Read sensor values for the line follower sensors
void read_line_sensors()
{
  digitalWrite(sensors_power, HIGH);
  left_sensor = digitalRead(left_sensor_pin);
  right_sensor = digitalRead(right_sensor_pin);
//  Serial.print(left_sensor);
//  Serial.println(right_sensor);
}

void halfTurn() {
  //This method turns the robot by 180 degrees after grabbing a block
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);

  rightMotor->setSpeed(150);
  leftMotor->setSpeed(150);

  /*
  //Executes half turn using set time duration
  const unsigned long start_time = millis();
  const long turn_duration = 3500;

  while(millis() - start_time <= turn_duration) {
    rightMotor->setSpeed(fwd_speed);
    leftMotor->setSpeed(fwd_speed);
   }
   */


  int turn_count = 0;
  
  while(turn_count < 4){

    read_line_sensors();
    if (!(left_sensor == old_left_sensor && right_sensor == old_right_sensor)){
      old_left_sensor = left_sensor;
      old_right_sensor = right_sensor;
      turn_count += 1;
      Serial.print("Turn count: ");
      Serial.println(turn_count);
      Serial.print(left_sensor);
      Serial.println(right_sensor);
      delay(100);
    }
  }
  
  Serial.println("Done");

  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  
  delay(5000);
}

void loop(){
  
  //Fix this in void loop()

  // get sensor data
  digitalWrite(sensors_power, HIGH);
  read_line_sensors();

  halfTurn();
}
