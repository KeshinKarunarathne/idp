#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Creating the Adafruit_MotorShield object
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);     // Creating the left DC motor object
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);    // Creating the right DC motor object

const int fwd_speed = 200;       // Default forward speed
const int turn_speed_slow = 180; // Speed of slower motor during a turn
const int turn_speed_fast = 220; // Speed of faster motor during a turn

void setup() 
{
    AFMS.begin(); // Connecting to the controller

    // Speed ranges from 0(stopped) to 255(full speed)
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
    leftMotor->setSpeed(fwd_speed);
    rightMotor->setSpeed(fwd_speed);
}

// Turn the robot to the right while still moving
void rightTurn()
{
  rightMotor->setSpeed(turn_speed_slow);
  leftMotor->setSpeed(turn_speed_fast);
}

// Turn the robot the left while moving
void leftTurn()
{
  rightMotor->setSpeed(turn_speed_fast);
  leftMotor->setSpeed(turn_speed_slow);
}

// Make the robot move in the direction its pointing
void goStraight()
{
  rightMotor->setSpeed(fwd_speed);
  leftMotor->setSpeed(fwd_speed);
}

void loop() {
    goStraight();
    delay(5000);
    leftTurn();
    delay(2000);
    goStraight();
    delay(5000);
    rightTurn();
    delay(2000);
}

