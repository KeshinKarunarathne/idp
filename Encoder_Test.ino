
const int encoder_pin = 2; // Encoder pin (has to be pin 2 for Arduino Uno)
const int encoder_holes = 36; // Number of holes in the encoder wheel
int encoder_count = 0; // Number of times the photointerrupter changes state
float distance_from_start = 0.0; // Distance from start, monitored by the rotary encoder
float encoder_wheel_radius; // Radius of encoder wheel

void setup() {
  Serial.begin(9600);
  
  pinMode(encoder_pin, INPUT);

  attachInterrupt(digitalPinToInterrupt(encoder_pin), encoder_counter, CHANGE);
}

void encoder_counter() {
  encoder_count++;
  Serial.print("Encoder Count: ");
  Serial.println(encoder_count);
}

float distance(){
  distance_from_start = 2*PI*encoder_wheel_radius*(encoder_count/(encoder_holes*2));
  return distance_from_start;
}

void loop() {
    //Testing the accuracy of the ISR when a for loop is running
    for(int i = 0; i <= 50; i++){
      Serial.println(i);
      delay(1000);
    }
   
//  Serial.print("Encoder_count:");
//  Serial.println(encoder_count);
//  Serial.print("Distance from start");
//  Serial.print(distance());
}
